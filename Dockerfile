ARG ELK_VERSION

FROM docker.elastic.co/kibana/kibana:${ELK_VERSION}

USER root

COPY validationWorkflow.zip /tmp/validationWorkflow.zip
RUN chown kibana:kibana /tmp/validationWorkflow.zip

USER kibana

RUN kibana-plugin install "file:///tmp/validationWorkflow.zip"
